import { Component, OnInit } from '@angular/core';
import { CabecalhoPaginaService } from '../../service/cabecalho-pagina/cabecalho-pagina.service';

@Component({
  selector: 'ui-element-cabecalho-pagina',
  templateUrl: './cabecalho-pagina.component.html',
  styleUrls: ['./cabecalho-pagina.component.scss']
})
export class CabecalhoPaginaComponent implements OnInit {

  constructor(private _cabecalhoPaginaService: CabecalhoPaginaService) { }

  public titulo: string;
  public icone: string;

  ngOnInit() {
    this._cabecalhoPaginaService.carregarCabecalhoPaginaComponent(this);
  }

}
