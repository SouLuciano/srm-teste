import { Component, OnInit, ViewChild } from '@angular/core';

import { ToastComponent } from '@syncfusion/ej2-angular-notifications';


import { isNullOrUndefined } from '@syncfusion/ej2-base';
import { NotificacaoService } from '../../service/notificacao/notificacao.service';
import { ENotificacaoModelo } from '../../enum/notificacao-lateral-modelo.enum';

@Component({
  selector: 'ui-element-notificacao-lateral',
  templateUrl: './notificacao-lateral.component.html',
  styleUrls: ['./notificacao-lateral.component.scss']
})
export class NotificacaoLateralComponent implements OnInit {

  @ViewChild('notificacaoLateral') public notificacaoLateral: ToastComponent;

  constructor(private _notificacaoService: NotificacaoService) { }

  ngOnInit() {
    this._notificacaoService.carregarNotificacaoLateralComponent(this);
  }

  /**
  * Cria o cartão de notificação na lateral da página.
  * @param  titulo Título do cartão.
  * @param  mensagem Corpo da mensagem.
  * @param  modelo Modelo do cartão: 'info', 'warning', 'success', 'danger'.
  * @param  tempo Permanencia do cartão na tela, infinito = 0.
  * @returns void
  */
  public notificar(titulo: string, mensagem: string, modelo: ENotificacaoModelo, tempo: number = 15000): void {

    let botaoFechar = false;
    let icone = this.setarIcone(modelo);
    let background = this.setarBackground(modelo);

    if (tempo === 0) {
      botaoFechar = true;
    }

    setTimeout(() => {
      this.notificacaoLateral.show({
        title: titulo,
        content: mensagem,
        timeOut: tempo,
        showProgressBar: true,
        position: { X: 'Right' },
        cssClass: background,
        icon: icone,
        showCloseButton: botaoFechar,
      });
    }, 200);
  }

  public onBeforeOpen(toast): void {
    let progress = toast.element.querySelector('.e-toast-progress');

    if (!isNullOrUndefined(progress)) {
      progress.style.backgroundColor = 'white';
    }
  }

  private setarBackground(modelo: ENotificacaoModelo): string {
    switch (modelo) {
      case ENotificacaoModelo.Info:
        return 'e-toast-info';
      case ENotificacaoModelo.Warning:
        return 'e-toast-warning';
      case ENotificacaoModelo.Success:
        return 'e-toast-success';
      case ENotificacaoModelo.Danger:
        return 'e-toast-danger';
    }
  }

  private setarIcone(modelo: ENotificacaoModelo): string {
    switch (modelo) {
      case ENotificacaoModelo.Info:
        return 'e-info toast-icons';
      case ENotificacaoModelo.Warning:
        return 'e-warning toast-icons';
      case ENotificacaoModelo.Success:
        return 'e-success toast-icons';
      case ENotificacaoModelo.Danger:
        return 'e-error toast-icons';
    }
  }
}
