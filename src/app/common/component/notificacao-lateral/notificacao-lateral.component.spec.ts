import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificacaoLateralComponent } from './notificacao-lateral.component';

describe('NotificacaoLateralComponent', () => {
  let component: NotificacaoLateralComponent;
  let fixture: ComponentFixture<NotificacaoLateralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificacaoLateralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificacaoLateralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
