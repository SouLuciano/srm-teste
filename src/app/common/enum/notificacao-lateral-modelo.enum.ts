export const enum ENotificacaoModelo {
  Info = 'info',
  Warning = 'warning',
  Success = 'success',
  Danger = 'danger'
}
