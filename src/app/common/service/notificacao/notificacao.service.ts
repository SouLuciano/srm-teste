import { Injectable } from '@angular/core';

import { NotificacaoLateralComponent } from '../../component/notificacao-lateral/notificacao-lateral.component';
import { ENotificacaoModelo } from '../../enum/notificacao-lateral-modelo.enum';


@Injectable()
export class NotificacaoService {

  private _notificacaoLateralComponent: NotificacaoLateralComponent;

  constructor() { }

  /**
  * Método interno do notificação Service.
  * @returns void
  */
  public carregarNotificacaoLateralComponent(component: NotificacaoLateralComponent): void {
    this._notificacaoLateralComponent = component;
  }

  /**
  * Cria o cartão de notificação na lateral da página.
  * @param titulo Título.
  * @param mensagem Corpo da mensagem.
  * @param modelo Modelo do cartão: 'info', 'warning', 'success', 'danger'.
  * @param tempo Permanencia do cartão na tela, infinito = 0.
  * @returns void
  */
  public notificar(titulo: string, mensagem: string, modelo: ENotificacaoModelo = ENotificacaoModelo.Info, tempo: number = 5000) {
    this._notificacaoLateralComponent.notificar(titulo, mensagem, modelo, tempo);
  }

}
