import { TestBed, inject } from '@angular/core/testing';

import { CabecalhoPaginaService } from './cabecalho-pagina.service';

describe('CabecalhoPaginaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CabecalhoPaginaService]
    });
  });

  it('should be created', inject([CabecalhoPaginaService], (service: CabecalhoPaginaService) => {
    expect(service).toBeTruthy();
  }));
});
