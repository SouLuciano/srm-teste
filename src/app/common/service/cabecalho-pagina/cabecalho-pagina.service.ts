import { Injectable } from '@angular/core';
import { CabecalhoPaginaComponent } from '../../component/cabecalho-pagina/cabecalho-pagina.component';

@Injectable()
export class CabecalhoPaginaService {

  private _cabecalhoPaginaComponent: CabecalhoPaginaComponent;

  public carregarCabecalhoPaginaComponent(componenet: CabecalhoPaginaComponent): void {
    this._cabecalhoPaginaComponent = componenet;
  }

  public setarCabecalho(titulo: string, icone: string): void {
    this._cabecalhoPaginaComponent.titulo = titulo;
    this._cabecalhoPaginaComponent.icone = icone;
  }
}
