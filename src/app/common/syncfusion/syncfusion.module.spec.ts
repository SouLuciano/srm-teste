import { SyncfusionModule } from './syncfusion.module';

describe('SyncfusionModule', () => {
  let syncfusionModule: SyncfusionModule;

  beforeEach(() => {
    syncfusionModule = new SyncfusionModule();
  });

  it('should create an instance', () => {
    expect(syncfusionModule).toBeTruthy();
  });
});
