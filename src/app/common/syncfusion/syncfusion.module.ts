import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule, SwitchModule  } from '@syncfusion/ej2-angular-buttons';
import { DropDownListModule, MultiSelectAllModule } from '@syncfusion/ej2-angular-dropdowns';
import { MaskedTextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { DropDownButtonModule } from '@syncfusion/ej2-angular-splitbuttons';
import { TooltipModule } from '@syncfusion/ej2-angular-popups';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { TextBoxModule } from '@syncfusion/ej2-angular-inputs';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    DropDownButtonModule,
    TooltipModule,
    DropDownListModule,
    MaskedTextBoxModule,
    SwitchModule,
    DialogModule,
    TextBoxModule,
    MultiSelectAllModule
  ],
  exports: [
    ButtonModule,
    DropDownListModule,
    DropDownButtonModule,
    TooltipModule,
    MaskedTextBoxModule,
    SwitchModule,
    DialogModule,
    TextBoxModule,
    MultiSelectAllModule
  ]
})
export class SyncfusionModule {
 }
