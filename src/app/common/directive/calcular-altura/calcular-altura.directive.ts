import { Directive, ElementRef, AfterViewChecked, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[fixarCabecalhoContexto]'
})
export class CalcularAlturaCabecalhoDirective implements AfterViewChecked {

  @Input() fixarCabecalhoContexto: string;

  alturaCabecalho: number;

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngAfterViewChecked() {
    this.matchHeight(this.el.nativeElement, this.fixarCabecalhoContexto);
  }

  matchHeight(pai: HTMLElement, className: string) {

    let cabecalho = pai.getElementsByClassName(className.split(',')[0]);
    let altura = cabecalho[0].clientHeight + 15;
    this.alturaCabecalho = altura;
    let form = pai.getElementsByClassName(className.split(',')[1]);
    this.renderer.setStyle(form[0], 'height', `calc(100% - ${altura}px)`);
  }
}
