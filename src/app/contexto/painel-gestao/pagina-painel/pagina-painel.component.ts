import { Component, OnInit, ViewChild } from '@angular/core';
import { CabecalhoPaginaService } from '../../../common/service/cabecalho-pagina/cabecalho-pagina.service';
import { DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
import { NotificacaoService } from '../../../common/service/notificacao/notificacao.service';
import { ENotificacaoModelo } from '../../../common/enum/notificacao-lateral-modelo.enum';

@Component({
  selector: 'app-pagina-painel',
  templateUrl: './pagina-painel.component.html',
  styleUrls: ['./pagina-painel.component.scss']
})
export class PaginaPainelComponent implements OnInit {
  @ViewChild('sample')
  public listObj: DropDownListComponent;

  public acoesData: Object[] = [
    { Id: '1', Texto: 'Assinatura 1' },
    { Id: '2', Texto: 'Assinatura 2' },
    { Id: '3', Texto: 'Assinatura 3' },
    { Id: '4', Texto: 'Assinatura 4' },
    { Id: '5', Texto: 'Assinatura 5' },
    { Id: '6', Texto: 'Assinatura 6' },
    { Id: '7', Texto: 'Assinatura 7' },
    { Id: '8', Texto: 'Assinatura 8' },
    { Id: '9', Texto: 'Assinatura 9' },
    { Id: '10', Texto: 'Assinatura 10' }
  ];

  public notasData: Object[] = [
    { Id: '1', Texto: 'Nota 1' },
    { Id: '2', Texto: 'Nota 2' },
    { Id: '3', Texto: 'Nota 3' },
    { Id: '4', Texto: 'Nota 4' },
    { Id: '5', Texto: 'Nota 5' },
    { Id: '6', Texto: 'Nota 6' },
    { Id: '7', Texto: 'Nota 7' },
    { Id: '8', Texto: 'Nota 8' },
    { Id: '9', Texto: 'Nota 9' },
    { Id: '10', Texto: 'Nota 10' }
  ];

  public fields: Object = { text: 'Texto', value: 'Id' };

  constructor(
    private _cabecalhoPaginaService: CabecalhoPaginaService,
    private _notificacaoLateral: NotificacaoService
  ) {}

  ngOnInit() {
    this._cabecalhoPaginaService.setarCabecalho('Painel de Gestão', 'fa fa-globe fa-2x');
  }

  onChange(evento) {
    this._notificacaoLateral.notificar('Item Selecionado', evento.itemData.Texto, ENotificacaoModelo.Success);
  }

  onBtnClick(texto: string) {
    this._notificacaoLateral.notificar('Sucesso', `Você clicou em: ${texto}`);
  }
}
