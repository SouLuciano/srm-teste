import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaPainelComponent } from './pagina-painel.component';

describe('PaginaPainelComponent', () => {
  let component: PaginaPainelComponent;
  let fixture: ComponentFixture<PaginaPainelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaPainelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaPainelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
