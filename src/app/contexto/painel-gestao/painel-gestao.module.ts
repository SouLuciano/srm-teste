import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginaPainelComponent } from './pagina-painel/pagina-painel.component';
import { RouterModule } from '@angular/router';
import { PainelRoutes } from './routes/painel-gestao.routes';
import { SyncfusionModule } from '../../common/syncfusion/syncfusion.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PainelRoutes),
    SyncfusionModule
  ],
  declarations: [
    PaginaPainelComponent
  ]
})
export class PainelGestaoModule { }
