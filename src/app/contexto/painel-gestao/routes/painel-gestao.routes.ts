import { Routes } from '@angular/router';

import { PaginaPainelComponent } from '../pagina-painel/pagina-painel.component';

export const PainelRoutes: Routes = [
  { path: 'gestao', component: PaginaPainelComponent }
];
