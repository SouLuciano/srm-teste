import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuModule as MenuModuleSyncfusion, ToolbarModule, SidebarModule, TreeViewModule } from '@syncfusion/ej2-angular-navigations';
import { ToastModule } from '@syncfusion/ej2-angular-notifications';

import { MenuLateralComponent } from './menu-lateral/menu-lateral.component';
import { MenuSuperiorComponent } from './menu-superior/menu-superior.component';
import { PaginaInicialComponent } from './pagina-inicial/pagina-inicial.component';
import { SyncfusionModule } from '../../common/syncfusion/syncfusion.module';
import { CalcularAlturaCabecalhoDirective } from '../../common/directive/calcular-altura/calcular-altura.directive';
import { CabecalhoPaginaComponent } from '../../common/component/cabecalho-pagina/cabecalho-pagina.component';
import { ContextoRoutingModule } from '../contexto-routing.module';
import { CabecalhoPaginaService } from '../../common/service/cabecalho-pagina/cabecalho-pagina.service';
import { NotificacaoLateralComponent } from '../../common/component/notificacao-lateral/notificacao-lateral.component';
import { NotificacaoService } from '../../common/service/notificacao/notificacao.service';

@NgModule({
  imports: [
    CommonModule,
    ContextoRoutingModule,
    ToastModule,
    SidebarModule,
    MenuModuleSyncfusion,
    ToolbarModule,
    TreeViewModule,
    SyncfusionModule
  ],
  declarations: [
    MenuLateralComponent,
    MenuSuperiorComponent,
    PaginaInicialComponent,
    CabecalhoPaginaComponent,
    NotificacaoLateralComponent
  ],
  providers: [
    CalcularAlturaCabecalhoDirective,
    CabecalhoPaginaService,
    NotificacaoService
  ]
})
export class MenuModule { }
