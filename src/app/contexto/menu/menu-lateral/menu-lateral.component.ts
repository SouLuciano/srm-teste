import { Component, ViewChild, OnInit } from '@angular/core';

import { SidebarComponent, NodeSelectEventArgs } from '@syncfusion/ej2-angular-navigations';
import { Router } from '@angular/router';
import { NotificacaoService } from '../../../common/service/notificacao/notificacao.service';
import { ENotificacaoModelo } from '../../../common/enum/notificacao-lateral-modelo.enum';

@Component({
  selector: 'ui-element-menu-lateral',
  templateUrl: './menu-lateral.component.html',
  styleUrls: ['./menu-lateral.component.scss']
})
export class MenuLateralComponent implements OnInit {
  @ViewChild('menuLateral') public menuLateral: SidebarComponent;

  public menuLateralData: Object[] = [
    {
      nodeId: '01',
      nodeText: 'Painel de Gestão',
      iconCss: 'fa fa-globe fa-2x'
    },
    {
      nodeId: '02',
      nodeText: 'Controle Digital',
      iconCss: 'fa fa-usd'
    },
    {
      nodeId: '03',
      nodeText: 'Ant. de Recebíveis',
      iconCss: 'fa fa-area-chart',
      nodeChild: [
        { nodeId: '03-01', nodeText: 'Pagina 1', iconCss: 'fa fa-eercast' },
        { nodeId: '03-02', nodeText: 'Pagina 2', iconCss: 'fa fa-ravelry' }
      ]
    }
  ];

  public field: Object = {
    dataSource: this.menuLateralData,
    id: 'nodeId',
    text: 'nodeText',
    child: 'nodeChild',
    iconCss: 'iconCss'
  };

  constructor(private _router: Router, private _notificacaoLateral: NotificacaoService) {}

  ngOnInit() {
    this.menuLateral.show();
    this.menuLateral.target = '#contexto';
  }

  onSelecionado(nodeSelecionado: NodeSelectEventArgs) {
    if (nodeSelecionado.nodeData.text === 'Painel de Gestão') {
      this._router.navigate(['painel/gestao']);
    }
    if (nodeSelecionado.nodeData.text === 'Controle Digital') {
      this._router.navigate(['controle/digital']);
    }
    if (nodeSelecionado.nodeData.text === 'Pagina 1') {
      this._notificacaoLateral.notificar(
        'Redirecionado de Pagina 1',
        'Link Pagina 1 de demostração, Você foi redirecionado para pagina Painel de Gestão.',
        ENotificacaoModelo.Warning
      );
      this._router.navigate(['painel/gestao']);
    }
    if (nodeSelecionado.nodeData.text === 'Pagina 2') {
      this._notificacaoLateral.notificar(
        'Redirecionado de Pagina 2',
        'Você foi redirecionado para pagina Painel de Gestão.',
        ENotificacaoModelo.Warning
      );
      this._router.navigate(['painel/gestao']);
    }
  }
}
