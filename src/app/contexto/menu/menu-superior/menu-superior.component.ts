import { Component, OnInit, ViewChild } from '@angular/core';
import { ButtonComponent } from '@syncfusion/ej2-angular-buttons';
import { DropDownButtonComponent } from '@syncfusion/ej2-angular-splitbuttons';
import { interval } from 'rxjs/observable/interval';


@Component({
  selector: 'ui-element-menu-superior',
  templateUrl: './menu-superior.component.html',
  styleUrls: ['./menu-superior.component.scss']
})
export class MenuSuperiorComponent implements OnInit {
  public menuDropdownUsuario: any = '#menuSuperior-dropdownUsuario';
  public menuDropdownOpcoes: any = '#menuSuperior-dropdownOpcoes';
  public menuNotificacao: any = '#menuSuperior-notificacao';
  public notificacaoQtd: number | string;
  public titulo = '<div class = "e-tool-name" style="font-size: 15px;"> Grupo Econômico: </div>';

  @ViewChild('dropdownbutton') public dropdownbutton: DropDownButtonComponent;
  @ViewChild('menuItemButton') public menuItemButton: ButtonComponent;


  public dropdownOpcoesData: object = [
    { id: 'menuSuperior-dropdownOpcao1', text: 'Opção 1'},
    { id: 'menuSuperior-dropdownOpcao2', text: 'Opção 2' }
  ];

  public dropdownUsuarioData: object = [
    { id: 'menuSuperior-dropdownConfig', text: 'Configurações', iconCss: 'fa fa-cog' },
    { id: 'menuSuperior-dropdownSair', text: 'Sair', iconCss: 'fa fa-power-off' }
  ];


  constructor() {}

  ngOnInit() {
    const source = interval(500);
    const subscribe = source.subscribe(val => {
      this.notificacaoQtd = val;
      if (val === 99) {
        subscribe.unsubscribe();
        this.notificacaoQtd = '99+';
      }
    });
  }


}
