import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginaControleComponent } from './pagina-controle/pagina-controle.component';
import { RouterModule } from '@angular/router';
import { ControleRoutes } from './routes/controle-digital.routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ControleRoutes),
  ],
  declarations: [
    PaginaControleComponent
  ]
})
export class ControleDigitalModule { }
