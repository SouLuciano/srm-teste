import { Component, OnInit } from '@angular/core';
import { CabecalhoPaginaService } from '../../../common/service/cabecalho-pagina/cabecalho-pagina.service';

@Component({
  selector: 'app-pagina-controle',
  templateUrl: './pagina-controle.component.html',
  styleUrls: ['./pagina-controle.component.scss']
})
export class PaginaControleComponent implements OnInit {

  constructor(private _cabecalhoPaginaService: CabecalhoPaginaService) { }

  ngOnInit() {
    this._cabecalhoPaginaService.setarCabecalho('Controle Digital', 'fa fa-usd fa-2x');
  }

}
