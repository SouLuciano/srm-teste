import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaControleComponent } from './pagina-controle.component';

describe('PaginaControleComponent', () => {
  let component: PaginaControleComponent;
  let fixture: ComponentFixture<PaginaControleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaControleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaControleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
