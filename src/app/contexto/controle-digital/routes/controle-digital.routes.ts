import { Routes } from '@angular/router';
import { PaginaControleComponent } from '../pagina-controle/pagina-controle.component';


export const ControleRoutes: Routes = [
  { path: 'digital', component: PaginaControleComponent }
];
