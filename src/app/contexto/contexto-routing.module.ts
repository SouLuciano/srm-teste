import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaginaInicialComponent } from './menu/pagina-inicial/pagina-inicial.component';

const contextoRoutes: Routes = [
  {
    path: '',
    component: PaginaInicialComponent,
    children: [
      {
        path: 'painel',
        loadChildren: '../contexto/painel-gestao/painel-gestao.module#PainelGestaoModule'
      },
      {
        path: 'controle',
        loadChildren: '../contexto/controle-digital/controle-digital.module#ControleDigitalModule'
      }
    ]
  }
];



@NgModule({
  imports: [RouterModule.forChild(contextoRoutes)],
  exports: [RouterModule]
})
export class ContextoRoutingModule {}
